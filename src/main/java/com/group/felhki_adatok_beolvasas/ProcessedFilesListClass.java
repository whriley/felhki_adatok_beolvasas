
package com.group.felhki_adatok_beolvasas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
import static com.group.felhki_adatok_beolvasas.GUI.logger;
/**
 *
 * @author peterr
 */
public class ProcessedFilesListClass {
    
    
    public Boolean ProcessedFilesListClass(String filepath, String searchedFile)
    {
        System.out.println("Feldolgozott FKI listából ellenőrzés!");
        //File file = new File("c:\\Users\\peterr\\Documents\\NetBeansProjects\\FELHKI_adatok_beolvasas\\processed.txt");
        File file = new File(filepath);
        Boolean found = false;
        try {
            Scanner scanner = new Scanner(file);
            System.out.println(filepath+" listaban a "+searchedFile+" keresése...");
            logger.info(filepath+" listaban a "+searchedFile+" keresése...");
            int lineNum = 0;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lineNum++;
                if(line.contains(searchedFile)) { 
                    logger.warn("A feldolgozottak kozt mar letezik a "+lineNum+" sorban!");
                    System.out.println("A feldolgozottak kozt mar letezik a "+lineNum+" sorban!");
                    found = true;
                    return found;
                }
            }
        } catch(FileNotFoundException e) { 
            System.out.println("A feldolgozott FKI fajlok listaja fajl nem talalhato: "+filepath+" - "+e);
            logger.error("A feldolgozott FKI fajlok listaja fajl nem talalhato: "+filepath+" - "+e);
        }
        return found;
    }
    
    public void addToProcessed(String filepath, String newdata)
    {
    try
        {    
            FileWriter fw = new FileWriter(filepath, true); 
            BufferedWriter bw = new BufferedWriter(fw);
            String lineToAppend = newdata+"\n";    
            bw.write(lineToAppend);
            bw.close();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}

