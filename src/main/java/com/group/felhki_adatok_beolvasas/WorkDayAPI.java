package com.group.felhki_adatok_beolvasas;

import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.io.FileReader;
import java.io.IOException;
import java.time.DayOfWeek;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.group.felhki_adatok_beolvasas.GUI.logger;

public class WorkDayAPI 
{
    boolean isTodayWorkDay = true; // Az alapértelmezett érték true legyen, ha hiba történik a fájl beolvasásakor
    String pathJSON = "";
    String yearJSONString = "";
    
    public WorkDayAPI(String JSONpath) 
    {
        pathJSON = JSONpath;
        System.out.println("Hasznalt WorkDay JSON: "+pathJSON);
        try (BufferedReader reader = new BufferedReader(new FileReader(pathJSON))) {
            StringBuilder jsonContent = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                jsonContent.append(line);
            }
            JSONObject jsonObject = new JSONObject(jsonContent.toString());
            String response = jsonObject.getString("response");
            int yearJSON = jsonObject.getInt("year");
            yearJSONString = String.valueOf(yearJSON);
            // logger.debug("YearJSON: "+yearJSONString);
            JSONArray days = jsonObject.getJSONArray("days");
            
            LocalDate today = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String todayString = today.format(formatter);        
            
            DayOfWeek dayOfWeek = today.getDayOfWeek();
            // Alapértelmezett feltételezés: munkanap
            isTodayWorkDay = true;
            
            // Ha szombat vagy vasárnap van, alapértelmezetten nem munkanap
            if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY) {
                isTodayWorkDay = false;
            }
            logger.info("Today: "+today);
            logger.debug("daysFromJSON: "+days.toString());
            // Vizsgálat a JSON fájl alapján
            for (int i = 0; i < days.length(); i++) {
                JSONObject day = days.getJSONObject(i);
                String date = day.getString("date");
                int type = day.getInt("type");
                
                // logger.debug("todayString :" + todayString + "?? date (JSON)" + date);
                // Ha a mai nap megegyezik egy JSON-beli nappal
                if (date.equals(todayString)) {
                    if (type == 1) {
                        // Ha munkaszüneti nap
                        isTodayWorkDay = false;
                        System.out.println("Munkaszüneti nap: JSON date: " + date);
                        logger.info("Munkaszüneti nap van: JSON date: " + date);
                    } else if (type == 2) {
                        // Ha áthelyezett munkanap
                        isTodayWorkDay = true;
                        System.out.println("Áthelyezett munkanap: JSON date: " + date);
                        logger.info("Áthelyezett munkanap van: JSON date: " + date);
                    }
                    break; // Kilépés, ha megtaláltuk a mai napot a JSON-ben
                }
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean getTodayIsWorkDay() {
        return isTodayWorkDay;
    }
    
    public boolean getYearEqualJSON() {
        LocalDate today = LocalDate.now();
        int year = today.getYear();
        String yearString = String.valueOf(year);
        System.out.println("Az aktuális év: " + yearString);
        
        if(yearString.equals(yearJSONString)) {
            System.out.println("Az aktuális év egyezik a munkaszüneti JSON fájlban szereplő évvel!");
            return true;
        } else {
            System.out.println("Az aktuális év NEM egyezik a munkaszüneti JSON fájlban szereplő évvel!");
            return false;
        }
    }
}
