package com.group.felhki_adatok_beolvasas;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import javax.swing.JComponent;
import java.util.Objects; // null vizsgálathoz a fájl listánál

import static com.group.felhki_adatok_beolvasas.GUI.logger;
import java.util.ArrayList;

/** Vizsgáló osztály, a megadott kimeneti mappa írhatóságának vizsgálata, FKI fájl lista készítés
 * @author peterr
 */
public class PreWorks extends JComponent {
        
    String outputFKIPath;
    ArrayList<File> FKIFinished;

    public PreWorks()
    {
            
    }
        
    public PreWorks(String outputFKIPath) {
        this.outputFKIPath = outputFKIPath;
    }
   
    public boolean checkDirectoryAvailable(String directoryPath)
    {
        File directory = new File(directoryPath);
        if (directory.exists()){
            logger.info(directoryPath +" - Elerhető!");
            return true;
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }
        else
        {
            logger.error(directoryPath +" - Nem létezik, vagy nem elerhető!");
            return false;
        }
    }
    
    /** Fileok listáját elkészítő metódus.
    * Megvizsgálja az adott mappát és File[] tömb listát készít az .fki kiterjesztésű fájlokból
    * @param FKIDirPath vizsgálandó mappa
    * @param fileExtension Szűrés fájlkiterjesztésre, csak a megadott fájltípust figyeli a mappában
    * @return File tömb fájlok listája a megadott mappából
    */
    public File[] FKIFajlokLista(File FKIDirPath, String fileExtension)
    {
        FilenameFilter fkiFilenameFilter = new FilenameFilter(){
        public boolean accept(File dir, String name) {
            String lowercaseName = name.toLowerCase();
            if (lowercaseName.endsWith(fileExtension)) {
               return true;
            } else {
               return false;
            }
          }
        };        
        File FKIFilesList[] = FKIDirPath.listFiles(fkiFilenameFilter);            
        System.out.println(fileExtension+" fajlok listája a "+FKIDirPath.getAbsolutePath()+" mappában:");
        logger.info(fileExtension+" fajlok listája a "+FKIDirPath.getAbsolutePath()+" mappában:");
        if(!(Objects.isNull(FKIFilesList)))
        {
            for(File fileName : FKIFilesList) 
            {
                System.out.println(fileName.getAbsoluteFile());
                logger.info(fileName.getAbsoluteFile());
            }
            return FKIFilesList;
        }
        else
        {
            logger.info("PreWorks osztaly => Objects.isNull(FKIDirPath.listFiles(fkiFilenameFilter)");
            System.out.println("Nincs "+fileExtension+" fájl a megadott mappában!");
            logger.warn("Nincs "+fileExtension+" fájl a megadott mappában!");
            return null;
        }
    }
    
    /** Mappát hoz létre az adott helyen.
    * @param directoryName elérési út és létrehozandó mappa neve
    * @return if directory was created, true, otherwise false
    * @throws IOException i/o hiba
    */
    public Boolean createDir(String directoryName) throws IOException
    {
        File directory = new File(directoryName);
        Boolean dirCreated = false;
        if (! directory.exists()){
            dirCreated=directory.mkdir();
            logger.info(directoryName +" - Nem létezik, létrehozás! (PreWorks class)");
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }
        return dirCreated;
    }
    
}


