package com.group.felhki_adatok_beolvasas;

import static com.group.felhki_adatok_beolvasas.GUI.logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Script futtató osztály. EmailPSScriptPath - Script elérési útja,
 * attachmentFilePath - csatolmány elérési útja az email küldéséhez. Az osztály
 * a 2 paraméter alapján elindítja a powershell scriptet loggolva. A script
 * paramétert (argumentumot) kap ami az csatolmány elérési útja. A többit a
 * scriptben kell módosítani (feladó, címzett, üzenetszövege stb.).
 *
 * @author peterr
 */
public class EmailSenderPowerShell {

    public EmailSenderPowerShell(String emailPSScriptPath, String attachmentFilePath) {
        System.out.println("EmailSenderPowerShell emailPSScriptPath value:" + emailPSScriptPath);
        logger.info("PowerShell Email Script PATH: " + emailPSScriptPath);
        System.out.println("EmailSenderPowerShell attachmentFilePath value: " + attachmentFilePath);
        logger.info("EmailSenderPowerShell attachmentFilePath value:" + attachmentFilePath);
        try {
            String command = "powershell.exe \"" + emailPSScriptPath + "\" \"" + attachmentFilePath + "\"";
            // Getting the version
            // String command = "powershell.exe  $env:username";
            // Executing the command
            logger.info("Powershell cmd line: " + command);
            Process powerShellProcess = Runtime.getRuntime().exec(command);
            // Getting the results
            powerShellProcess.getOutputStream().close();
            String line;
            System.out.println("Standard Output:");
            logger.info("PowerShell - Standard Output:");
            BufferedReader stdout = new BufferedReader(new InputStreamReader(
                    powerShellProcess.getInputStream()));
            String psOut = "";
            while ((line = stdout.readLine()) != null) {
               psOut = psOut+line+System.getProperty("line.separator");
            }
            if(!(psOut==""))
            {
                System.out.println(psOut);
                logger.info(psOut);
            }
            stdout.close();
            System.out.println("PowerShell - Standard Error:");
            logger.info("PowerShell - Standard Error:");
            BufferedReader stderr = new BufferedReader(new InputStreamReader(
                    powerShellProcess.getErrorStream()));
	    String psError = "";
            while ((line = stderr.readLine()) != null) {
		psError = psError+line+System.getProperty("line.separator");
            }
            if(!(psError==""))
            {
                System.out.println(psError);
                logger.error(psError);
            }
            stderr.close();
            System.out.println("Done");
            logger.info("Done");
        } catch (IOException ioex) {
            System.out.println(ioex);
        }
    }
}
