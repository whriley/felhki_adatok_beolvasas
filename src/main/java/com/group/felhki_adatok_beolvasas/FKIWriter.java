package com.group.felhki_adatok_beolvasas;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.group.felhki_adatok_beolvasas.GUI.logger;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.NoSuchFileException;

/** Fájl másoló osztály, illetve ha szükséges mappa létrehozása
 * 
 *
 * @author peterr
 */
public class FKIWriter {
    
    String fromFile = "";
    String toFile = "";

/** Indító metódus.
 * 
 * @param fromFile forrás fájl helye (pl. d:\..\forras.fki)
 * @param toFile cél fájl helye (pl. d:\..\cel.fki$)
 * @author peterr
 */
    public FKIWriter(String fromFile, String toFile) {
        
        try {
            copyFileNIO(fromFile, toFile);
        } catch (IOException e) {
            logger.error("FKIWriter class: "+e);
            e.printStackTrace();
            
        }
        System.out.println(toFile+" fájl átmásolva!");
        logger.info(toFile+" fájl átmásolva!");
    }

/** Átmásolja a fájlt.
 * 
 * @param from forrás fájl helye (pl. d:\..\forras.fki)
 * @param to cél fájl helye (pl. d:\..\cel.fki$)
 * @author peterr
 * @throws IOException i/o hiba
 */
    public static void copyFileNIO(String from, String to) throws IOException {

        Path fromFile = Paths.get(from);
        Path toFile = Paths.get(to);
        
        // if fromFile doesn't exist, Files.copy throws NoSuchFileException
        if (Files.notExists(fromFile)) {
            System.out.println("FKI fájl nem létezik? Ellenőrizze az FKI fájl elérhetőségét!" + fromFile);
            logger.warn("FKI fájl nem létezik? Ellenőrizze az FKI fájl elérhetőségét! " + fromFile);
            return;
        }
        
        // if toFile folder doesn't exist, Files.copy throws NoSuchFileException
        // if toFile parent folder doesn't exist, create it.
        // tartalék mappa létrehozás (ha mégse sikerült volna)
        Path parent = toFile.getParent();
        if(parent!=null){
            if(Files.notExists(parent))
            {
                Files.createDirectories(parent);
                logger.info(parent +" Nem létezik, létrehozás! [FKIWriter.copyFileNIO()]"); 
            }
        }
        // multiple StandardCopyOption
        CopyOption[] options = { StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES,
                LinkOption.NOFOLLOW_LINKS };
        
        Files.copy(fromFile, toFile, options);
        // default - if toFile exist, throws FileAlreadyExistsException
        // Files.copy(fromFile, toFile);
        
        // if toFile exist, replace it.
        // Files.copy(fromFile, toFile, StandardCopyOption.REPLACE_EXISTING);
        
    }      
}

       

     

