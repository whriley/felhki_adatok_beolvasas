package com.group.felhki_adatok_beolvasas;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static com.group.felhki_adatok_beolvasas.GUI.logger;
import java.io.FileInputStream;

/** Properties File kezelő osztály.
 *
 * Ez a fájl tartalmazza a program számára szükséges változókat, amit külső properties fájlként elérhető.
 * "felhkiProp.properties" helye, az a root mappa amiben a target is van.
 * 
 * @author Réti János, Péter Richárd
 */
public class PropertiesControll {

    Properties prop;
    String inputPath = "";
    String defoutputPath = "";
    String emailPSScriptPath = "";
    String ResultFile = "";
    String WorkdayJSONPath = "";
    //the base folder is ./, the root of the main.properties file  
    String propFileName = "./felhkiProp.properties";
    
    FileInputStream inputStream;

    public void PropertiesControll() throws IOException{
        try {
                prop = new Properties();
                //load the file handle for main.properties
                inputStream = new FileInputStream(propFileName);
                if (inputStream != null) {
                        prop.load(inputStream);
                } else {
                        throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                }
                // get the property value and print it out
                inputPath = prop.getProperty("inputPath");
                defoutputPath = prop.getProperty("defoutputPath");
                emailPSScriptPath = prop.getProperty("emailPSScriptPath");
                ResultFile = prop.getProperty("ResultFile");
                WorkdayJSONPath = prop.getProperty("WorkdayJSONPath");
        } catch (Exception e) {
                System.out.println("Exception: " + e);
                logger.info("propertiesControll hiba: "+e);
        } finally {
                inputStream.close();
        }
    }
    
/** A getInputPath függvény.
 *  Az inputPath értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */
    public String getInputPath() {
        return inputPath;
    }
/** A getResultFile függvény.
 *  Az ResultFile értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */
    public String getResultFile() {
        return ResultFile;
    }
/** A getDefoutputPath függvény.
 *  Az defoutputPath értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */
    public String getDefoutputPath() {
        return defoutputPath;
    }
/** A getEmailPSScriptPath függvény.
 *  Az emailPSScriptPath értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */    
    public String getEmailPSScriptPath() {
        return emailPSScriptPath;
    }
/** A getWorkdayURLWithAPISec függvény.
 *  Az WorkdayURLWithAPISec értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */     
    public String getWorkdayJSONPath() {
        return WorkdayJSONPath;
    }
/** A propFileName függvény.
 *  Az propFileName értéket adja oda a hívónak ki a properties fájlból.
 *  @return String
 */    
     public String propFileName()
    {
        return propFileName;
    }
}
    