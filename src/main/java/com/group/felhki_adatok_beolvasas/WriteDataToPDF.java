package com.group.felhki_adatok_beolvasas;

import static com.group.felhki_adatok_beolvasas.GUI.logger;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;
    /**
    * PDF fájl készítő a feldolgozott Felhki egyedekből, az eredemény fájl yyyyMMdd-HHmmss.pdf nevű lesz.
    * 
    * @version 1.0
    * @since   2021-05-21
    */
    public class WriteDataToPDF {
    
    String fileName = "";
    FELHKI felhki; // Egy felhki fájl reprezentálása
    Map < String, FELHKI > felhkiFileok; // Összes felhki fájl
    String outPath; // Excel file mentési helye d:\\..\\..\\ formában
    Document document = new Document(PageSize.A4.rotate()); // A4-es méretre állítása és forgatása fekvőre
    float[] widths = {0.13f, 0.025f, 0.1f, 0.1f, 0.27f, 0.15f, 0.1f, 0.125f}; // Oszlopok szélességek (százalékosan)
    /**
    * WriteDataToPDF osztály.
    * PDF fájlt készít a feldolgozott Felhki egyedekből, az eredemény fájl yyyyMMdd-HHmmss.pdf formátum lesz.
    * 
    * @param outputPath feldolgozott fki fájlok helye, generált eredmény fájlok helye
    * @author  Péter Richárd, Réti János
    * @version 1.0
    * @since   2021-05-20 
    */
    public WriteDataToPDF(String outputPath)
    {
        this.outPath = outputPath;
    }
    /**
    * Start függvény indítja el a feldolgozást.
    * @param felhkiFileok ugyanaz mint a felhkiObjects, de fájlnév szerint rendezve (ami a dátum rendezés)
    * @throws IOException i/o hiba
    * @throws DocumentException PDF dokumentum hiba
    */
    public void start(Map<String, FELHKI> felhkiFileok) throws IOException, DocumentException
    {
        fileName = outPath+getDate()+".pdf";
        PdfWriter.getInstance(document, new FileOutputStream(fileName));
        document.open();
        Iterator it = felhkiFileok.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry)it.next();
            felhki = (FELHKI) pair.getValue();
            PdfPTable table = new PdfPTable(widths);
            addTableHeader(table, (String) pair.getKey(), felhki.getUzenetSorszamString(), felhki.getuzenetOsszeallitasDatuma().toString(), felhki.getUzenetOsszeallitasanakIdeje().toString(), felhki.getSzolgaltatoKod());
            // Tételek a FELHKI-ből
            ArrayList tetelek = felhki.getFelhkiTetelList();
            Iterator iter = tetelek.iterator();
            document.add(table);
            while (iter.hasNext())
            {
                FELHKI_TETEL tetel = (FELHKI_TETEL) iter.next();
                // System.out.println(pair.getKey() + " = " + pair.getValue());
                felhki = (FELHKI) pair.getValue();
                try{
                    addRows(tetel);                    
                }catch(DocumentException de)
                {
                    System.err.println("Nem sikerült hozzáadni a táblázatot a PDF fájlhoz! --> "+de);
                    logger.error("Nem sikerült hozzáadni a táblázatot a PDF fájlhoz! --> "+de);
                }
            }
        }
        document.addCreationDate();
        document.addAuthor("IT Group Tszol");
        document.close();
        System.out.println(fileName + " sikeresen mentve!");
        logger.info(fileName + " sikeresen mentve!");
    }
    /**
    * addTableHeader függvény a kapott táblázatnak készít fejlécet.
    * @param PdfPTable table - ugyanaz mint a felhkiObjects, de fájlnév szerint rendezve (ami a dátum rendezés)
    * @param columnHeaderFirst - A Felhki fájlból kiolvasott név és/vagy egyéb fejléc adat
    * @param columnHeaderSecond- A Felhki fájlból kiolvasott név és/vagy egyéb fejléc adat
    * @param columnHeaderThird - A Felhki fájlból kiolvasott név és/vagy egyéb fejléc adat
    * @param columnHeaderFourth - A Felhki fájlból kiolvasott név és/vagy egyéb fejléc adat
    * @param columnHeaderFifth - A Felhki fájlból kiolvasott név és/vagy egyéb fejléc adat
    */
    private void addTableHeader(PdfPTable table, String columnHeaderFirst, String columnHeaderSecond, String columnHeaderThird, String columnHeaderFourth, String columnHeaderFifth) {
    Stream.of(columnHeaderFirst, "", "", "", columnHeaderSecond, columnHeaderThird, columnHeaderFourth, columnHeaderFifth)
      .forEach(columnTitle -> {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(2);
        header.setPhrase(new Phrase(columnTitle));
        table.addCell(header);
    });
    }
    /**
    * addRows függvény a tételt illeszti be a táblázat celláiba.
    * 3 sorra van bontva egy tétel, és itt rögzítve van, hogy ne törje új oldalra ezt szét.
    * Végén ez a függvény adja hozzá a dokumentumhoz, nem ahonnan meg lett hívva.
    * @param tetel egy tétel összes adata
    */
    private void addRows(FELHKI_TETEL tetel) throws DocumentException {
    PdfPTable tableFirst = new PdfPTable(widths);
    tableFirst.setSplitLate(false);
    tableFirst.setKeepTogether(true);    
    tableFirst.getDefaultCell().setBorder(0); // Tabla szegelyek torlese (Header kivetel)

    PdfPCell cell;
    cell = new PdfPCell(new Phrase(tetel.getTetelSorszam().toString()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);
    
    cell = new PdfPCell(new Phrase(tetel.getFelhatalmazasJellegeString()));
    cell.setBorder(PdfPCell.TOP); // cellaknak csak felso szegely meghagyasa
    tableFirst.addCell(cell);

    cell = new PdfPCell(new Phrase(tetel.getFelhatalmazasJellegeMegjegyzessel()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);

    cell = new PdfPCell(new Phrase(tetel.getFogyasztoAzonositoja()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);

    cell = new PdfPCell(new Phrase(tetel.getKotelezettBankszamlaSzama()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);

    cell = new PdfPCell(new Phrase(tetel.getKotelezettNeve()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);
    
    cell = new PdfPCell(new Phrase(tetel.getFelhatalmazasErvenyessegenekKezdete()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);

    cell = new PdfPCell(new Phrase(tetel.getFelhatalmazasErtekhataraString()));
    cell.setBorder(PdfPCell.TOP);
    tableFirst.addCell(cell);
    
    // Új sor
    tableFirst.addCell("");
    tableFirst.addCell("");
    tableFirst.addCell("");
    tableFirst.addCell("");
    tableFirst.addCell("");
    tableFirst.addCell(tetel.getFogyasztoNeve());
    tableFirst.addCell(tetel.getFelhatalmazasErvenyessegenekVege());
    tableFirst.addCell("");
    
    // Új sor
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(tetel.getFogyasztoCim()));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(tetel.getFelhatalmazasKelte()));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    cell = new PdfPCell(new Phrase(""));
    cell.setBorder(PdfPCell.BOTTOM);
    tableFirst.addCell(cell);
    
    document.add(tableFirst); // Mind 3 sor (1 felhki tetel) hozzaadasa a pdfhez
    }
            
    /**
    * getDate függvény generál nevet a kimeneti fájlhoz.
    * @return String
    */  
    public String getDate()
    {
        Date date = new Date(); // This object contains the current date value
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return formatter.format(date).toString();
    }

                
    /**
    * A függvény visszadja a hívónak az elkészült PDF fájlnevet.
    * @return String
    */
    public String getFileName() {
        return fileName;
    }
    
}
