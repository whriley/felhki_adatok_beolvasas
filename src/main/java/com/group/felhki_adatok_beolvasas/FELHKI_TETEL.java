package com.group.felhki_adatok_beolvasas;

    /** Egy Felhki tétel reprezentálása.
     * 
     * @author peterr
     */
    public class FELHKI_TETEL {
        int tetelTipusFELHKITetelnek = 0;
        String tetelSorszam = "";
        char felhatalmazasJellege = '-';
        String szolgaltatoAzonositoja = "" ;
        String fogyasztoAzonositoja = "";
        String kotelezettBankszamlaSzama = "";
        String kotelezettNeve = "";
        String felhatalmazasErvenyessegenekKezdete = "";
        String felhatalmazasErvenyessegenekVege = "";
        String felhatalmazasKelte = "";
        long felhatalmazasErtekhatara = 0;
        String fogyasztoNeve = "";
        String fogyasztoCim = "";
        String kozlemeny = "";

    public FELHKI_TETEL(int tetelTipusFELHKITetelnek, String tetelSorszam, char felhatalmazasJellege, String szolgaltatoAzonositoja, String fogyasztoAzonositoja, String kotelezettBankszamlaSzama, String kotelezettNeve, String felhatalmazasErvenyessegenekKezdete, String felhatalmazasErvenyessegenekVege, String felhatalmazasKelte, long felhatalmazasErtekhatara, String fogyasztoNeve, String fogyasztoCim, String kozlemeny) {
        this.tetelTipusFELHKITetelnek = tetelTipusFELHKITetelnek;
        this.tetelSorszam = tetelSorszam;
        this.felhatalmazasJellege = felhatalmazasJellege;
        this.szolgaltatoAzonositoja = szolgaltatoAzonositoja;
        this.fogyasztoAzonositoja = fogyasztoAzonositoja;
        this.kotelezettBankszamlaSzama = kotelezettBankszamlaSzama;
        this.kotelezettNeve = kotelezettNeve;
        this.felhatalmazasErvenyessegenekKezdete = felhatalmazasErvenyessegenekKezdete;
        this.felhatalmazasErvenyessegenekVege = felhatalmazasErvenyessegenekVege;
        this.felhatalmazasKelte = felhatalmazasKelte;
        this.felhatalmazasErtekhatara = felhatalmazasErtekhatara;
        this.fogyasztoNeve = fogyasztoNeve;
        this.fogyasztoCim = fogyasztoCim;
        this.kozlemeny = kozlemeny;
    }
    
    /** Szöveges értelmezés a felhatalmazás jellegre.
     * U/T/D/L/M karaktereket értelmezi kis-nagy betűként is vizsgálja.
     * @author peterr
     * @return String
     */
    public String getFelhatalmazasJellegeMegjegyzessel()
    {
        if(this.felhatalmazasJellege == 'U' || this.felhatalmazasJellege == 'u')
        {
            return "Új";
        }
        if(this.felhatalmazasJellege == 'T' || this.felhatalmazasJellege == 't')
        {
            return "Törlés";
        }
        if(this.felhatalmazasJellege == 'D' || this.felhatalmazasJellege == 'd')
        {
            return "é.vége/mód.";
        }
        if(this.felhatalmazasJellege == 'L' || this.felhatalmazasJellege == 'l')
        {
            return "limit mód.";
        }
        if(this.felhatalmazasJellege == 'M' || this.felhatalmazasJellege == 'm')
        {
            return "é.vége/limit mód.";
        }
        else
        {
            return "Hibás, csak U/T/D/L/M lehet!";
        }
    }
    
    public String getFelhatalmazasJellegeString() {
        return (String.valueOf(felhatalmazasJellege));
    }

    public int getTetelTipus() {
        return tetelTipusFELHKITetelnek;
    }

    public void setTetelTipus(int tetelTipusFELHKITetelnek) {
        this.tetelTipusFELHKITetelnek = tetelTipusFELHKITetelnek;
    }

    public String getTetelSorszam() {
        return tetelSorszam;
    }

    public void setTetelSorszam(String tetelSorszam) {
        this.tetelSorszam = tetelSorszam;
    }

    public char getFelhatalmazasJellege() {
        return felhatalmazasJellege;
    }
    
    public void setFelhatalmazasJellege(char felhatalmazasJellege) {
        this.felhatalmazasJellege = felhatalmazasJellege;
    }

    public String getSzolgaltatoAzonositoja() {
        return szolgaltatoAzonositoja;
    }

    public void setSzolgaltatoAzonositoja(String szolgaltatoAzonositoja) {
        this.szolgaltatoAzonositoja = szolgaltatoAzonositoja;
    }

    public String getFogyasztoAzonositoja() {
        return fogyasztoAzonositoja;
    }

    public void setFogyasztoAzonositoja(String fogyasztoAzonositoja) {
        this.fogyasztoAzonositoja = fogyasztoAzonositoja;
    }

    public String getKotelezettBankszamlaSzama() {
        return kotelezettBankszamlaSzama;
    }

    public void setKotelezettBankszamlaSzama(String kotelezettBankszamlaSzama) {
        this.kotelezettBankszamlaSzama = kotelezettBankszamlaSzama;
    }

    public String getKotelezettNeve() {
        return kotelezettNeve;
    }

    public void setKotelezettNeve(String kotelezettNeve) {
        this.kotelezettNeve = kotelezettNeve;
    }

    public String getFelhatalmazasErvenyessegenekKezdete() {
        return felhatalmazasErvenyessegenekKezdete;
    }

    public void setFelhatalmazasErvenyessegenekKezdete(String felhatalmazasErvenyessegenekKezdete) {
        this.felhatalmazasErvenyessegenekKezdete = felhatalmazasErvenyessegenekKezdete;
    }

    public String getFelhatalmazasErvenyessegenekVege() {
        return felhatalmazasErvenyessegenekVege;
    }

    public void setFelhatalmazasErvenyessegenekVege(String felhatalmazasErvenyessegenekVege) {
        this.felhatalmazasErvenyessegenekVege = felhatalmazasErvenyessegenekVege;
    }

    public String getFelhatalmazasKelte() {
        return felhatalmazasKelte;
    }

    public void setFelhatalmazasKelte(String felhatalmazasKelte) {
        this.felhatalmazasKelte = felhatalmazasKelte;
    }

    public long getFelhatalmazasErtekhatara() {
        return felhatalmazasErtekhatara;
    }
    
    public String getFelhatalmazasErtekhataraString() {
        return String.valueOf(felhatalmazasErtekhatara);
    }

    public void setFelhatalmazasErtekhatara(int felhatalmazasErtekhatara) {
        this.felhatalmazasErtekhatara = felhatalmazasErtekhatara;
    }

    public String getFogyasztoNeve() {
        return fogyasztoNeve;
    }

    public void setFogyasztoNeve(String fogyasztoNeve) {
        this.fogyasztoNeve = fogyasztoNeve;
    }

    public String getFogyasztoCim() {
        return fogyasztoCim;
    }

    public void setFogyasztoCim(String fogyasztoCim) {
        this.fogyasztoCim = fogyasztoCim;
    }

    public String getKozlemeny() {
        return kozlemeny;
    }

    public void setKozlemeny(String kozlemeny) {
        this.kozlemeny = kozlemeny;
    }
    
    public String toString()
    {
        return getTetelTipus()+" - "+getTetelSorszam()+" - "+getKotelezettNeve();
    }
}
