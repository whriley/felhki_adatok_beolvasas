package com.group.felhki_adatok_beolvasas;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/** Egy FKI (FELHKI) fájl reprezentációja.
 *
 * @author peterr
 */
public class FELHKI {
    
    int duplumKod; // 1
    long UzenetSorszam; // 12 - Összeállítás dátuma (8), Sorszam (4) 
    String ido; // óóppmm
    LocalTime uzenetOsszeallitasanakIdeje;
    LocalDate uzenetOsszeallitasDatuma;
    String szolgaltatoKod; // 13
    ArrayList<FELHKI_TETEL> felhkiTetelList = new ArrayList<FELHKI_TETEL>();
    
    
    public FELHKI (int dumplumKod, long UzenetSorszam, String StrIdo, String szolgaltatoKod)
    {
        this.duplumKod = dumplumKod; // 1
        this.UzenetSorszam = UzenetSorszam; // 12 - Összeállítás dátuma (8), Sorszam (4) 
        this.ido = StrIdo; // óóppmm
        this.szolgaltatoKod = szolgaltatoKod; // 13
        
        String UzenetSorszamDate = String.valueOf(UzenetSorszam);
        UzenetSorszamDate = UzenetSorszamDate.substring(0,8);
        uzenetOsszeallitasDatuma = LocalDate.parse(UzenetSorszamDate, DateTimeFormatter.ofPattern("yyyyMMdd"));
        uzenetOsszeallitasanakIdeje = LocalTime.parse(StrIdo, DateTimeFormatter.ofPattern("HHmmss"));
    
        // System.out.println(uzenetOsszeallitasanakIdeje);
        // System.out.println(uzenetOsszeallitasDatuma);
    }
    public void AddfelhkiTetel(FELHKI_TETEL NewTetel)
    {
        this.felhkiTetelList.add(NewTetel);
    }

    public ArrayList<FELHKI_TETEL> getFelhkiTetelList() {
        return felhkiTetelList;
    }

    public void setFelhkiTetelList(ArrayList<FELHKI_TETEL> felhkiTetelList) {
        this.felhkiTetelList = felhkiTetelList;
    }
    
    public int getDuplumKod() {
        return duplumKod;
    }

    public void setDuplumKod(int duplumKod) {
        this.duplumKod = duplumKod;
    }

    public long getUzenetSorszam() {
        return UzenetSorszam;
    }
    
    public String getUzenetSorszamString() {
        return (String.valueOf(UzenetSorszam));
    }

    public void setUzenetSorszam(long UzenetSorszam) {
        this.UzenetSorszam = UzenetSorszam;
    }
    
    public String getSzolgaltatoKod() {
        return szolgaltatoKod;
    }

    public void setSzolgaltatoKod(String szolgaltatoKod) {
        this.szolgaltatoKod = szolgaltatoKod;
    }

    public LocalTime getUzenetOsszeallitasanakIdeje() {
        return uzenetOsszeallitasanakIdeje;
    }

    public void setUzenetOsszeallitasanakIdeje(LocalTime uzenetOsszeallitasanakIdeje) {
        this.uzenetOsszeallitasanakIdeje = uzenetOsszeallitasanakIdeje;
    }

    public LocalDate getuzenetOsszeallitasDatuma() {
        return uzenetOsszeallitasDatuma;
    }

    public void setuzenetOsszeallitasDatuma(LocalDate uzenetosszeallitasdatuma) {
        this.uzenetOsszeallitasDatuma = uzenetosszeallitasdatuma;
    }
    
}
