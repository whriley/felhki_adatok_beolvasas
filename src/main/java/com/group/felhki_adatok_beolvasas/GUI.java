package com.group.felhki_adatok_beolvasas;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
* Felhki fájl beolvasó (konvertáló) program indítófelülete (vezérlő felülete).
* <p>
* /AUTORUN: A kapcsolóval (argumentummal) indítva nincs GUI, feldolgozza a FKI fájlokat, PDF és XLSX is készül, majd a PSScriptet futtatja (megkapva a pdf fájl elérési útját).
* <p>
* Kimeneti mappa menüpont defoutputPath, startApp függvény ezt módosítja a véglegesre, amúgy az alapértelmezett mappa. Az alapértelmezett mappa a felhkiProp.properties fájlból olvasódik ki.
* Ha nincs fki kiterjesztésű fájl a bemeneti mappában, akkor a Feldolgozás gomb inaktív lesz és nem futtatható.
* Ha van találat, akkor a konstruktorban megnézzük az FKI fájlokat a bemeneti mappában, és ezt indításkor megjeleníti a felületen. Itt még nem módosít semmit a program.
* Majd a már végrehajtott FKI fájlok listáját összehasonlítjuk az új FKI fájlokkal. Ha nem szerepel a listában akkor berakjuk a feldolgozandó listába.
* A szürt feldolgozandó fájlistát beolvassa, HashMapbe kerül, rendezzük, és GUI felülettől függően generálunk XLSX és/vagy PDF fájlt.
* "FKIFajlLista[]" tartalmazza a fájl név listát, "felhkiObjects" felhki fájlok FELHKI objektumként reprezentálva, a "sortedMap" ugyanaz mint a felhkiObjects, de fájlnév szerint rendezve (ami a dátum rendezés). Ezután a kimeneti fájl/fájlok generálás a GUI felülettől függően.
* Ebben az osztályban a "logger" változót importálja a többi osztály is, log4j2.xml konfiguráció xml-ben az src/main/resources/log4j2.xml néven kell lennie, innen, a netbeansben biztosan elérhető.
* A System.out át van irányítva a GUI felületen megjelenő TextArea mezőben. Részletesebben a log fájlban található bejegyzés.
* Log fájl a parancssorban álló könyvtárhoz képest /log/felhki.log lesz!
* Exit code: 0 - Sikeres futás
* Exit code: 1 - WorkDayAPI eredénye szerint munkaszüneti nap van futás megszakítva (/autorun, /useWorkDayAPI)
* Exit code: 2 - Nem található FKI fájl a mappában!
* Exit code: 3 - Mappa nem elérhető vagy nem létezik!
* Exit code: 4 - Hiba! Az autorun nem indult el! Fatal Error!
* 
* @author  Péter Richárd, Réti János
* @version 1.0
* @since   2022-02-07
*/
public class GUI extends javax.swing.JFrame {

    String inputPath = ""; // Bemeneti mappa 
    String defoutputPath = "";  // Kimeneti mappa
    static final Logger logger = LogManager.getLogger(GUI.class); // Log4j
    File[] FKIFajlLista = null; // FKI fájlok listája, a végén feldolgozandó fájlok listája
    ArrayList<File> FKIFajlListaFinished = new ArrayList<File>(); // FKI fájlok listája, szűréshez segédlista
    PreWorks pw = new PreWorks(); 
    ProcessedFilesListClass processed; // Feldogozzott FKI fájlok neveit kezelő osztály 
    static boolean autorun = false;
    static boolean useWorkDayApi = false;
    static String argument0; // Első Parancssori arg. 
    static String argument1; // Második Parancssori arg. 
    String PdfFileName = ""; // Kimeneti PDF fájl neve, amit a PS Script megkap email küldésre
    String emailPSScriptPath = ""; // Futtatandó PS Script fájl helye
    String ResultFile = ""; // Lista a feldolgozott FKI fájl nevekről (Ehhez hasonlítjuk, hogy kiszűrjük a többszöri feldolgozást)
    String WorkdayJSONPath = ""; // Munkaszüneti Nap ellenőrzésre JSON elérési út
    JTextArea textArea;
        
    public GUI() 
    {
        initComponents();
        // Properties file olvasás
        String properFileName ="";
        try{
            PropertiesControll proper = new PropertiesControll();
            proper.PropertiesControll();
            properFileName = proper.propFileName();
            inputPath = proper.getInputPath();
            defoutputPath = proper.getDefoutputPath();
            emailPSScriptPath = proper.getEmailPSScriptPath();
            ResultFile = proper.getResultFile();
            WorkdayJSONPath = proper.getWorkdayJSONPath();
        }catch(IOException e)
        {
            logger.error("Properties file nem található! Filename: "+properFileName);
            logger.error(e);
        }
        System.out.println("FKI fajl feldolgozó indítása.... Várjon!");
        logger.info("====================================================================================");
        logger.info("FKI fajl feldolgozó indítása.... Várjon!");
        logger.info("Program elindítva!  getClassPath: "+getClassPath());
        JTextArea textArea = new JTextArea (25, 80);
        textArea.setEditable (false);
        jScrollPane1.add(textArea);
        JTextAreaOutputStream out = new JTextAreaOutputStream (textArea);
        textArea.setText("Alapértelmezett FKI mappa: "+inputPath+"\nAlapértelmezett eredmény mappa: "+defoutputPath+"\n");
        logger.info("Alapertelmezett FKI mappa: "+inputPath);
        logger.info("Alapertelmezett eredmeny mappa: "+defoutputPath);
        textArea.setFont(textArea.getFont().deriveFont(15f));
        Container contentPane = this.getContentPane ();
        contentPane.setLayout (new FlowLayout (3));
        contentPane.add (
            new JScrollPane (
                textArea, 
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED),
            BorderLayout.CENTER);
        this.pack();
        this.setSize(1100, 670);
        
        // System.out átírányítása a textArea-ba, hogy látható legyen a GUI-n, ha nincs 1. argumentum (pl. /AUTORUN)
        if(argument0 == null)
        {
            System.setOut (new PrintStream (out));
        }
        
        // Itt ha GUI kell megjelenik, vagy ha autorun akkor is megjeleníti valamelyik ponton ha nem jók a mappák
        PreWorks pw = new PreWorks(inputPath); 
        boolean inputPathFolderAvailable = pw.checkDirectoryAvailable(inputPath);
        boolean outputPathFolderAvailable = pw.checkDirectoryAvailable(defoutputPath);
        // Ha mindkét mappa elérhető akkor futtatjuk tovább egyáltalán a programot
        if(inputPathFolderAvailable == true && outputPathFolderAvailable == true)
        {
            File inputFile = new File(inputPath);
            //File outputFile = new File(defoutputPath);
            FKIFajlLista = pw.FKIFajlokLista(inputFile, ".fki");
            // Feldolgozattak kereses a fajlban, fajlnevek alapjan atvizsgaljuk, majd a Finishedbe rakjuk ami új
            processed = new ProcessedFilesListClass();
            for (File FKI : FKIFajlLista)
            {
                if(!(processed.ProcessedFilesListClass(ResultFile, FKI.getName())))
                {
                    FKIFajlListaFinished.add(FKI);
                    System.out.println("Feldolgozasi listaba rakva (FKIFajlListaFinished): "+FKI.getName());
                }
                else
                {
                    logger.warn("Már feldolgozott FKI: "+FKI.getAbsolutePath());
                }
            }
            // FKIFajlLista alapból File típusú tömb, FKIFajlListaFinished dinamikus bepakolás miatt ArrayList át kell alakítani File tömbre
            FKIFajlLista = FKIFajlListaFinished.toArray(new File[0]);
            for (File FKI : FKIFajlLista) {
                System.out.println("Feldolgozando FKI listaja:");
                System.out.println(FKI.getName());
            }
            
            // Ha nincs fki fájl hibát dobna ezért már itt megvizsgáljuk null-e a lista
            if(FKIFajlLista.length > 0)
            {
                lbAllapot.setText("Fájlok száma: "+FKIFajlLista.length);
                logger.info("Fájlok száma: "+FKIFajlLista.length);
            }
            if(FKIFajlLista.length < 1)
            {
                lbAllapot.setText("Fájlok száma: nincs fájl");
                logger.error("Fájlok száma: nincs fájl");
                System.out.println("Nem található FKI fájl a mappában!");
                btFeldolgozas.setEnabled(false);
            }
            argRunner();
        }
        else{
            if(inputPathFolderAvailable == false)
            {
                System.out.println(inputPath +" mappa nem elérhető vagy nem létezik!");
                logger.error(inputPath +" mappa nem elérhető vagy nem létezik!");
                btFeldolgozas.setEnabled(false);
                // Ha nincs meg a mappa, és ha autorun van lépjünk is ki
                if(argument0 != null)
                {
                    logger.error("Exit code: 3");
                    System.exit(3);
                }
            }
            if(outputPathFolderAvailable == false)
            {
                System.out.println(defoutputPath +" mappa nem elérhető vagy nem létezik!");
                logger.error(defoutputPath +" mappa nem elérhető vagy nem létezik!");
                btFeldolgozas.setEnabled(false);
                // Ha nincs meg a mappa, és ha autorun van lépjünk is ki
                if(argument0 != null)
                {
                    logger.error("Exit code: 3");
                    System.exit(3);
                }
            }
        }
    }

    public void argRunner()
    {
        // Futás kiválasztása a futtatási opciók alapján
        if (autorun && useWorkDayApi) {
            // useWorkdayAPI kapcsoló true, használjuk a WorkDayAPI-t, megvizsgáljuk a mai nap munkanap-e
            logger.info("Autorun indítási mód - Autorun args included: "+autorun);
            System.out.println("Autorun indítási mód - Autorun args included:  "+autorun);
            logger.info("/useworkdayapi kapcsolo hasznalva, hasznalt JSON: "+WorkdayJSONPath);
            WorkDayAPI workday = new WorkDayAPI(WorkdayJSONPath);
            Boolean TodayIsWorkDay = workday.getTodayIsWorkDay();
            logger.info("Az aktuális év egyezik a munkaszüneti JSON fájlban szereplő évvel? "+workday.getYearEqualJSON());
            if(!(TodayIsWorkDay)){
                System.out.println("Mai nap nem munkanap a program leáll!");
                logger.info("Mai nap nem munkanap a program leáll! TodayIsWorkDay: "+TodayIsWorkDay);
                System.exit(1);
            }
            else {
                System.out.println("A mai nap munkanap: "+TodayIsWorkDay);
                logger.info("A mai nap munkanap: "+TodayIsWorkDay);
                try {
                    if(FKIfajlListaLengthChecker()) {
                        logger.debug("argAutorunRunner called. Call StartApp() method!");
                        startApp();
                        EmailSenderPowerShell emailPSSender = new EmailSenderPowerShell(emailPSScriptPath, PdfFileName);
                        logger.info("Exit code: 0");
                        System.exit(0);
                    }
                    else {
                        EmailSenderPowerShell emailPSSender = new EmailSenderPowerShell(emailPSScriptPath, "null");
                        logger.debug("argAutorunRunner called!");
                        logger.warn("Nem található FKI fájl a mappában! Length: "+FKIFajlLista.length);
                        logger.debug("FKIFajlLista.length value: "+FKIFajlLista.length);
                        logger.warn("Exit code: 2");
                        System.out.println("Nem található FKI fájl a mappában!");
                        System.exit(0);
                    }
                }
                catch(Exception ex) {
                    logger.error("Hiba! Az autorun nem indult el! Kapott parameter - argument0: "+argument0);
                    logger.error(ex);
                    System.out.println("Hiba! Az autorun nem indult el! Kapott parameter - argument0: "+argument0);
                    System.out.println(ex);
                    logger.error("Exit code: 4");
                    System.exit(4);
                }
            }
        } 
        else if (autorun) {
            try {
                logger.info("Autorun indítási mód - Autorun args included: "+autorun);
                System.out.println("Autorun indítási mód - Autorun args included:  "+autorun);
                if(FKIfajlListaLengthChecker())
                {
                    logger.debug("argAutorunRunner called. Call StartApp() method!");
                    startApp();
                    EmailSenderPowerShell emailPSSender = new EmailSenderPowerShell(emailPSScriptPath, PdfFileName);
                    logger.info("Exit code: 0");
                    System.exit(0);
                }
                else {
                    System.out.println("Nem található FKI fájl a mappában!");
                    EmailSenderPowerShell emailPSSender = new EmailSenderPowerShell(emailPSScriptPath, "null");
                    logger.debug("argAutorunRunner called, FKIfajlListaLengthChecker false erteket adott!");
                    logger.warn("Nem található FKI fájl a mappában!"+FKIFajlLista.length);
                    logger.debug("FKIFajlLista.length value: "+FKIFajlLista.length);
                    logger.warn("Exit code: 2");
                    System.exit(0);
                }
            }
            catch(Exception ex) {
                logger.error("Hiba! Az autorun nem indult el! Kapott parameter - argument0: "+argument0);
                logger.error(ex);
                System.out.println("Hiba! Az autorun nem indult el! Kapott parameter - argument0: "+argument0);
                System.out.println(ex);
                logger.error("Exit code: 4");
                System.exit(4);
            }
        } 
        else {
            // GUI indítás kell
        }
    }
        
    public boolean FKIfajlListaLengthChecker() {
        
        boolean result = false;
       
            if(FKIFajlLista.length > 0)
            {
                result = true;
                return result;
                /* startApp();
                EmailSenderPowerShell emailPSSender = new EmailSenderPowerShell(emailPSScriptPath, PdfFileName); */
            }
            if(FKIFajlLista.length < 1)
            {
                result = false;
                return result;
            }
        return result;
    }
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        btFeldolgozas = new javax.swing.JButton();
        btBezaras = new javax.swing.JButton();
        lbAllapot = new java.awt.Label();
        jChkPDF = new java.awt.Checkbox();
        jChkExcel = new java.awt.Checkbox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miBemenetiMappaMegnyitasa = new javax.swing.JMenuItem();
        miKimenetiMappaMegnyitasa = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("FKI fájl feldolgozó");
        setSize(new java.awt.Dimension(800, 500));
        getContentPane().setLayout(null);
        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(53, 16, 2, 2);

        btFeldolgozas.setText("Feldolgozás");
        btFeldolgozas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFeldolgozasActionPerformed(evt);
            }
        });
        getContentPane().add(btFeldolgozas);
        btFeldolgozas.setBounds(60, 5, 99, 25);

        btBezaras.setText("Bezárás");
        btBezaras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBezarasActionPerformed(evt);
            }
        });
        getContentPane().add(btBezaras);
        btBezaras.setBounds(164, 5, 77, 25);

        lbAllapot.setName(""); // NOI18N
        lbAllapot.setText("Fájlok száma:      ");
        getContentPane().add(lbAllapot);
        lbAllapot.setBounds(490, 10, 110, 20);
        lbAllapot.getAccessibleContext().setAccessibleName("Fájlok száma:                    ");

        jChkPDF.setLabel("PDF generálás");
        jChkPDF.setState(true);
        getContentPane().add(jChkPDF);
        jChkPDF.setBounds(250, 10, 109, 20);

        jChkExcel.setLabel("Excel generálás");
        jChkExcel.setState(true);
        getContentPane().add(jChkExcel);
        jChkExcel.setBounds(370, 10, 114, 20);

        jMenu1.setText("File");

        miBemenetiMappaMegnyitasa.setText("Bemeneti mappa megnyitása");
        miBemenetiMappaMegnyitasa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miBemenetiMappaMegnyitasaActionPerformed(evt);
            }
        });
        jMenu1.add(miBemenetiMappaMegnyitasa);

        miKimenetiMappaMegnyitasa.setText("Kimeneti mappa megnyitása");
        miKimenetiMappaMegnyitasa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miKimenetiMappaMegnyitasaActionPerformed(evt);
            }
        });
        jMenu1.add(miKimenetiMappaMegnyitasa);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btBezarasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBezarasActionPerformed
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_btBezarasActionPerformed

    private void btFeldolgozasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFeldolgozasActionPerformed
        try {
            startApp();
            btFeldolgozas.setEnabled(false);
        } catch (Exception ex) {
            logger.error("btFeldolgozasActionPerformed: Exception: "+ex);
        }
    }//GEN-LAST:event_btFeldolgozasActionPerformed

    private void miKimenetiMappaMegnyitasaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miKimenetiMappaMegnyitasaActionPerformed
        try {
            Desktop.getDesktop().open(new File(defoutputPath));
        } catch (IOException ex) {
            logger.error(ex);
        }
    }//GEN-LAST:event_miKimenetiMappaMegnyitasaActionPerformed

    private void miBemenetiMappaMegnyitasaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miBemenetiMappaMegnyitasaActionPerformed
        try {
                Desktop.getDesktop().open(new File(inputPath));
            } catch (IOException ex) {
                logger.error(ex);
            }
    }//GEN-LAST:event_miBemenetiMappaMegnyitasaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        
        // Argumentumok ellenőrzése
        for (String arg : args) {
            if (arg.equalsIgnoreCase("/autorun")) {
                autorun = true;
            } else if (arg.equalsIgnoreCase("/useworkdayapi")) {
                useWorkDayApi = true;
            }
        }
        
       /* if(args.length > 0)
        {
            for (int i = 0; i < args.length; i++) {
                System.out.println("Argument " + (i) + ": " + args[i]);
                logger.info("Argument " + (i) + ": " + args[i]);
            }
            argument0 = args[0];
            if(args.length == 2)
            {
                argument1 = args[1];
                logger.info("Argument1: "+argument1);
            }
        }
        */
            /* Create and display the form */
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new GUI().setVisible(true);
                }
            });
    }
    /**
    * startApp függvény indítja el programot.
    * Kiemeneti üzenetek a log fájlban, és a GUI felületen jelennek meg.
    * @throws Exception mappa, vagy feldolgozás kivétel
    */
    public void startApp () throws Exception
    {
        HashMap<String, FELHKI> felhkiObjects;
        PreWorks pwOut = new PreWorks(defoutputPath);
        DateFormat dateFormat = new SimpleDateFormat("yyMM");
        Date date = new Date();
        defoutputPath = defoutputPath+dateFormat.format(date)+"\\";
        logger.debug("defoutputPath value: "+defoutputPath);
        try{
            File directory = new File(defoutputPath);
            Boolean dirCreated = false;
            if (! directory.exists()){
                dirCreated = pw.createDir(defoutputPath);
                if(dirCreated == false)
                {
                    logger.error(defoutputPath +" mappát nem sikerült létrehozni!");
                }
                if(dirCreated == true)
                {
                    logger.info(defoutputPath +" mappa sikeresen létrehozva!");
                }
            }
            if(directory.exists())
            {
                logger.info(defoutputPath +" - mappa létezik!");
            }
        }catch(Exception iex)
        {
            System.out.println("Mappát nem sikerült létrehozni: "+iex);
            logger.error(defoutputPath +" mappát nem sikerült létrehozni: "+iex);
        }
         
        // Példányosítjuk az FELHKI olvasási osztályt
        ReadFilesLineByLine reading = new ReadFilesLineByLine(); 
        // Beállítjuk hova mentse a kész FKI-t
        reading.setOutFKIPath(defoutputPath);
        // A FKI fájlok listáját átadjuk, majd elkezdjük olvasni őket
        felhkiObjects = reading.read(FKIFajlLista);
        // TestWriter(felhkiObjects);
        // Rendezés Filename szerint, hogy a kimeneti fájlokban (PDF, Excel) dátum szerint legyen
            Map<String, FELHKI> sortedMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int lengthDifference = o1.length() - o2.length();
                if (lengthDifference != 0) return lengthDifference;
                return o1.compareTo(o2);
            }
        });
        // sortedMap alapból rendezve rakja be az értékeket tehát csak berakjuk a kész HashMapet ide        
        sortedMap.putAll(felhkiObjects);
        // Excel fájl generálás elindítása ha be van jelölve (Default: true)
        logger.debug("jChkExcel.getState() allapota:"+jChkExcel.getState());
        if(jChkExcel.getState()==true)
        {
            logger.info("Excel fájl generálás elinditva!");
            try 
            {
                WriteDataToExcelFile wd = new WriteDataToExcelFile();
                wd.setOutPath(defoutputPath);
                wd.start(sortedMap);
            }catch(IOException e)
            {
                System.out.println(e.getMessage());
                logger.error("Hiba a WriteDataToExcel osztályban! - "+e.getMessage());
            }
        }
        // PDF fájl generálás elindítása ha be van jelölve (Default: true)
        logger.debug("jChkPdf.getState() allapota:"+jChkPDF.getState());
        if(jChkPDF.getState()==true)
        {
            logger.info("PDF fájl generálás elinditva!");        
            try{
                WriteDataToPDF wdPdf = new WriteDataToPDF(defoutputPath);
                wdPdf.start(sortedMap);
                PdfFileName = wdPdf.getFileName();
                logger.info("Generalt PDF file: "+PdfFileName);
            }catch(Exception e)
            {
                System.out.println(e.getMessage());
                logger.error("Hiba a WriteDataToPDFosztályban! - "+e.getMessage());
            }
            System.out.println("Feldolgozás sikeres! Eredmény a "+defoutputPath+" helyen található!");
        }
        // A kész FKI fájl törlése a bemeneti mappából
        SourceFkiDelete(FKIFajlLista);
        logger.info("Feldolgozás sikeres! Eredmény a [defoutpath] "+defoutputPath+" helyen található!");
    }
    /**
    * Törli a File elemtípusú tömbe lévő fájlokat a bemeneti mappából.
    * Ha törlésig eljutunk, akkor feltételezzük, hogy nem volt hiba a feldolgozás közben. 
    * Ezért töröljük és ha az is sikerült, a feldolgozást bejegyezzük az eredmény fájlba, ahonnan a legközelebbi futás vizsgálja a bejegyzéseket.
    * @param FKIFajlLista FKI fájlok listája, a törlendő eredeti FKI fájlok az bemeneti mappából
    */
    public void SourceFkiDelete(File FKIFajlLista[])
    {
        for (int i = 0; i < FKIFajlLista.length; i++) {
            // accessing each element of array
            FKIFajlLista[i].delete();
            if(!FKIFajlLista[i].exists())
            {
                processed.addToProcessed(ResultFile ,FKIFajlLista[i].getName());
                logger.info(FKIFajlLista[i]+" ResultFile-ba küldve!");
            }
            else
            {
                logger.info(FKIFajlLista[i]+" - Exists: " + FKIFajlLista[i].exists());
                System.out.println(FKIFajlLista[i]+" FKI törlése nem sikerült!");
                logger.error(FKIFajlLista[i]+" - Törlése nem került a Processed ResultFileba! ResultFile: "+ResultFile);
            }
            logger.info(FKIFajlLista[i]+" - Exists: " + FKIFajlLista[i].exists());
        }
    }
    /**
    * Futtatási hely felderítése.
    * A függvény visszaadja azt az elérési utat, ahonnan a program indul.
    * @return String típusú elérési út
    */
    public static String getClassPath() 
    {
        String classpath = "";
        URL resource = Thread.currentThread().getContextClassLoader().getResource("");
        if (resource != null) {
            classpath = resource.getPath();
        }
        return classpath;
    }
   /**
    * Teszt céljából kiírja az objektum értékeket [NINCS HASZNÁLVA].
    * @param felhkiObjects felhki fájlok FELHKI objektumként reprezentálva
    * Nincs kezelve ha nem dolgozunk fel FELHKI fájlt mert nem találtunk.
    */
    public static void TestWriter(HashMap felhkiObjects)
    {
        FELHKI felhkiFileAktualis;
        Iterator it = felhkiObjects.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry)it.next();
            felhkiFileAktualis = (FELHKI) felhkiObjects.get(pair.getKey());
            ArrayList<FELHKI_TETEL> felhkiFileAktualisTetelei = felhkiFileAktualis.getFelhkiTetelList();
            // System.out.println("Aktuális FELHKI file: "+pair.getKey()+" TETELEK:"+felhkiFileAktualisTetelei);
            logger.info("Aktuális FELHKI file: "+pair.getKey()+" TETELEK:"+felhkiFileAktualisTetelei);
        }    
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBezaras;
    private javax.swing.JButton btFeldolgozas;
    private java.awt.Checkbox jChkExcel;
    private java.awt.Checkbox jChkPDF;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label lbAllapot;
    private javax.swing.JMenuItem miBemenetiMappaMegnyitasa;
    private javax.swing.JMenuItem miKimenetiMappaMegnyitasa;
    // End of variables declaration//GEN-END:variables

}
