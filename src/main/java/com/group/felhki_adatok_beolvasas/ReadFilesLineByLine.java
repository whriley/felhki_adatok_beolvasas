package com.group.felhki_adatok_beolvasas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import static com.group.felhki_adatok_beolvasas.GUI.logger; // GUI osztályból logger változó

/** Felhki fájlok feldolgozása, karakterenként, és átmásolja a fájlokat az outputPath helyre.
 * @author peterr
 */

public class ReadFilesLineByLine {
    
    private static int i = 0;
    private static String tetelTipus = "";
    private static char[] strLineArray;
    private static String uzenetTipus = "";
    private static String outFKIPath = "";
    private ArrayList felhkiHibaList;
    HashMap<String, FELHKI> felhkiList = new HashMap<String, FELHKI>();
    String UzenetSorszamStr = "";
    
   /** Beolvasó és igazából feldolgozást elindító függvény.
    * Megvizsgálja minden fájl első 2 karakterét aminek "01" stringnek kell lennie, majd "FELHKI" szöveggel kell folytatódnia, különben nem lesz a fájl feldolgozva.
    * Majd amit feldolgozott azt átmásolja a megadott helyre.<p>
    * A karakter kódolása IBM852 a fájlnak, ezért a neveknél kódolást végez. <p>
    * Előszőr kiszedi a karaktereket a kellő hosszban, majd byte tömböt készít, utána átalakítja a jó kódolásra. <p>
    * <i>Jelenleg az "é" karakter byte kódja, kérdőjelként (63-as byte kód) jelenik meg string beolvasáskor, ezért egy replace metódus cseréli "é"-re!</i>
    * @param FKIFilenameList Fki fájlok tömbje
    * @author peterr
    * @return feldolgozott fki fájlok map adatai
    * @throws IOException i/o hiba
    */
    public HashMap read(File[] FKIFilenameList) throws IOException
    {
        ArrayList felhkiHibaList = new ArrayList();
        for(File filename : FKIFilenameList) 
        {
            // Open the file
            FileInputStream fstream = new FileInputStream(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            int duplumKod;
            long UzenetSorszam;
            String szolgaltatoKod;
            
                while ((strLine = br.readLine()) != null) 
                { 
                    // Minimum az első 2 karakter vizsgálható legyen különben kimarad mert (1 karakter értelmezhetetlen)
                    if(strLine.length()>1)
                    {
                    i++;
                    tetelTipus = "";
                    strLineArray = null;
                    strLineArray = strLine.toCharArray();
                    switch(tetelTipus = tetelTipus+strLineArray[0]+strLineArray[1]) {
                        case "01":
                        try{
                        // Vizsgálni a FELHKI-t is 
                            uzenetTipus=characterKiszedes(2,8, strLineArray); 
                            logger.info("Readed File: "+filename.getName()+" -> tetelTipus: "+tetelTipus+"uzenetTipus: "+uzenetTipus);
                            if(uzenetTipus.equals("FELHKI"))
                            {
                                duplumKod = Integer.parseInt(characterKiszedes(8,9, strLineArray));
                                UzenetSorszamStr = characterKiszedes(9,21, strLineArray);
                                String StrIdo = "";
                                StrIdo = characterKiszedes(21,27, strLineArray);
                                szolgaltatoKod = characterKiszedes(27,strLine.length(), strLineArray);
                                szolgaltatoKod = szolgaltatoKod.trim();
                                UzenetSorszamStr = UzenetSorszamStr.trim();
                                UzenetSorszam = Long.parseLong(UzenetSorszamStr);
                                /*
                                System.out.println ("duplumKod: "+duplumKod);
                                System.out.println ("UzenetSorszam: "+UzenetSorszam);
                                System.out.println ("StrIdo: "+StrIdo);
                                System.out.println ("szolgaltatoKod: "+szolgaltatoKod);
                                */
                                felhkiList.put(filename.getName(),new FELHKI(duplumKod,UzenetSorszam,StrIdo,szolgaltatoKod));
                            }
                            else{
                                felhkiHibaList.add(filename.getName()+";Első sornak a ÜzenetTípusa (F131 mező) nem FELHKI! Lehet rossz a fájl, nem FKI!");
                                System.out.println(filename.getName()+" ---> Első sornak a ÜzenetTípusa (F131 mező) nem FELHKI! Lehet rossz a fájl, nem FKI!");
                                logger.error(filename.getName()+" ---> Első sornak az ÜzenetTípusa (F131 mező) nem FELHKI! Lehet rossz a fájl, nem FKI!");
                                
                            }
                            }catch(Exception e)
                            {
                                felhkiHibaList.add(filename.getName()+";Fájl formátuma nem megfelelő!;(01FELHKI hiányzik)");
                                System.out.println("Az "+filename.getName()+" fájl formátuma nem megfelelő! (01FELHKI hiányzik) ---> "+e);
                                logger.error("Az "+filename.getName()+" fájl formátuma nem megfelelő! (01FELHKI hiányzik) ---> Exception result: "+e);
                            }
                            break;
                        case "02":
                            System.err.println(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet alcsoport feje! KIHAGYÁS!");
                            logger.info(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet alcsoport feje! KIHAGYÁS!");
                            break;
                        case "03":
                            try{
                            int tetelTipusFELHKITetelnek;
                            String tetelSorszam;
                            char felhatalmazasJellege;
                            String szolgaltatoAzonositoja;
                            String fogyasztoAzonositoja;
                            String kotelezettBankszamlaSzama;
                            String kotelezettNeve;
                            String felhatalmazasErvenyessegenekKezdete;
                            String felhatalmazasErvenyessegenekVege;
                            String felhatalmazasKelte;
                            long felhatalmazasErtekhatara;
                            String fogyasztoNeve;
                            String fogyasztoCim;
                            String kozlemeny;
                            tetelTipusFELHKITetelnek = Integer.parseInt(characterKiszedes(2,4, strLineArray));
                            tetelSorszam = characterKiszedes(4,10, strLineArray);
                            felhatalmazasJellege = characterKiszedes(10,11, strLineArray).charAt(0);
                            szolgaltatoAzonositoja = characterKiszedes(11,24, strLineArray).trim();
                            fogyasztoAzonositoja = characterKiszedes(24,47, strLineArray).trim();
                            kotelezettBankszamlaSzama = characterKiszedes(47,72, strLineArray).trim();
                            
                            kotelezettNeve = characterKiszedes(72,107, strLineArray).trim();
                            // Kódolás Cp852 (IBM852)
                            byte[] bytes = kotelezettNeve.getBytes(); // Kódoláshoz bytekódok kellenek
                            // Mivel az "é" karakter kérdőjel lesz, bytekódja "63" ami valóban kérdőjel, valamiért az É karakter ide így kerül (notepad++ jól mutatja)
                            kotelezettNeve = new String(bytes, "Cp852").replace("?", "é"); 
                            
                            felhatalmazasErvenyessegenekKezdete = characterKiszedes(107,115, strLineArray).trim();
                            felhatalmazasErvenyessegenekVege = characterKiszedes(115,123, strLineArray).trim();
                            felhatalmazasKelte = characterKiszedes(123,131, strLineArray).trim();
                            felhatalmazasErtekhatara = Long.parseLong(characterKiszedes(131,141, strLineArray).trim());

                            fogyasztoNeve = characterKiszedes(141,176, strLineArray).trim();
                            bytes = fogyasztoNeve.getBytes();
                            fogyasztoNeve = new String(bytes, "Cp852").replace("?", "é"); 
                            
                            fogyasztoCim = characterKiszedes(176,212, strLineArray).trim();
                            bytes = fogyasztoCim.getBytes();
                            fogyasztoCim = new String(bytes, "Cp852").replace("?", "é");
                            
                            kozlemeny = characterKiszedes(212,strLineArray.length, strLineArray).trim();
                            bytes = kozlemeny.getBytes();
                            kozlemeny = new String(bytes, "Cp852").replace("?", "é");
                            /*
                            System.out.println("szolgaltatoAzonositoja:"+szolgaltatoAzonositoja);
                            System.out.println("fogyasztoAzonositoja:"+fogyasztoAzonositoja);
                            System.out.println("kotelezettBankszamlaSzama:"+kotelezettBankszamlaSzama);
                            System.out.println("kotelezettNeve:"+kotelezettNeve);
                            System.out.println("felhatalmazasErvenyessegenekKezdete:"+felhatalmazasErvenyessegenekKezdete);
                            System.out.println("felhatalmazasErvenyessegenekVege:"+felhatalmazasErvenyessegenekVege);
                            System.out.println("felhatalmazasKelte:"+felhatalmazasKelte);
                            System.out.println("tetelTipusFELHKITetelnek:"+tetelTipusFELHKITetelnek);
                            System.out.println("tetelSorszam:"+tetelSorszam);
                            System.out.println("felhatalmazasJellege:"+felhatalmazasJellege);
                            System.out.println("felhatalmazasErtekhatara:"+felhatalmazasErtekhatara);
                            System.out.println("fogyasztoNeve:"+fogyasztoNeve);
                            System.out.println("fogyasztoCim:"+fogyasztoCim);
                            System.out.println("kozlemeny:"+kozlemeny);
                            */
                            FELHKI aktualFelhki = felhkiList.get(filename.getName());
                            aktualFelhki.AddfelhkiTetel(new FELHKI_TETEL (tetelTipusFELHKITetelnek, tetelSorszam, felhatalmazasJellege, szolgaltatoAzonositoja, fogyasztoAzonositoja, kotelezettBankszamlaSzama, kotelezettNeve, felhatalmazasErvenyessegenekKezdete, felhatalmazasErvenyessegenekVege, felhatalmazasKelte, felhatalmazasErtekhatara, fogyasztoNeve, fogyasztoCim, kozlemeny));                    
                            }catch(Exception ex)
                            {
                                felhkiHibaList.add(filename.getName()+";Tétel hiba - kézi ellenőrzés szükséges;"+ex);
                                System.out.println(filename.getName()+" TÉTEL HIBA az alábbi fájlban: "+filename.getName()+" Vizsgálat szükséges: ---> "+ex);
                                logger.error(filename.getName()+" TÉTEL HIBA az alábbi fájlban, vizsgálat szükséges! ---> Exception result: "+ex);
                            }
                            break;
                        case "04":
                            System.err.println(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet alcsoport láb! KIHAGYÁS!");
                            logger.info(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet alcsoport láb! KIHAGYÁS!");
                            break;
                        case "05":   
                            System.err.println(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet láb!");
                            logger.info(filename.getName()+" Tétel típus: "+tetelTipus+"FELHKI üzenet láb!");
                            FKIWriter fw = new FKIWriter(filename.getPath(), outFKIPath+filename.getName()+"$");
                            break;
                        default:
                            felhkiHibaList.add(filename.getName()+";Első sornak a TételTípusa (F130 mező) nem 01! Lehet rossz a fájl, nem FKI!");
                            logger.error(filename.getName()+";Első sornak a TételTípusa (F130 mező) nem 01! Lehet rossz a fájl, nem FKI!");
                        }
                    }
                    else
                    {
                        System.out.println(filename.getName()+" értelmezhetetlen sort tartalmaz, 0 vagy 1 karakter!");
                        logger.error(filename.getName()+" értelmezhetetlen sort tartalmaz, 0 vagy 1 karakter!");
                    }
                }   
                fstream.close();
        }
        return felhkiList;
    }
    
    /** Karaktereket string típusra alakít, a megadott intervallum közt.
    * @param start első vizsgált karakter helye
    * @param end utolsó vizsgált karakter helye
    * @param strLineArray a beolvasott sor karaktertömbje
    * @author peterr
    * @return kesz string
    */
    public static String characterKiszedes (int start, int end, char[] strLineArray)
    {
        String resultStr = "";
        for(int j=start;j!=end;j++)
        {
            resultStr = resultStr+strLineArray[j];
        }
        return resultStr;
    }

    public HashMap<String, FELHKI> getFelhkiList() {
        return felhkiList;
    }

    public void setFelhkiList(HashMap<String, FELHKI> felhkiList) {
        this.felhkiList = felhkiList;
    }
    
    public static String getOutFKIPath() {
        return outFKIPath;
    }

    public static void setOutFKIPath(String outPath) {
       outFKIPath = outPath;
    }

    public ArrayList getFelhkiHibaList() {
        return felhkiHibaList;
    }

    public void setFelhkiHibaList(ArrayList felhkiHibaList) {
        this.felhkiHibaList = felhkiHibaList;
    }

}
