package com.group.felhki_adatok_beolvasas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import static com.group.felhki_adatok_beolvasas.GUI.logger;


   /**
    * XLSX fájl készítő a feldolgozott Felhki egyedekből, az eredemény fájl yyyyMMdd-HHmmss.xlsx nevű lesz.
    * 
    * @version 1.0
    * @since   2021-05-20
    */
    class WriteDataToExcelFile {
    
    FELHKI felhki; // Egy felhki fájl reprezentálása
    Map < String, FELHKI > felhkiFileok; // Összes felhki fájl
    String outPath; // Excel file mentési helye d:\\..\\..\\ formában
    HashMap<String, String> felhkiHibaList; 
    //Create blank workbook    
    XSSFWorkbook workbook = new XSSFWorkbook();
    //Create a blank sheet
    XSSFSheet spreadsheet = workbook.createSheet("FKI_feldolgozott");
    //Create row object
    XSSFRow row;
    int rowid = 0;
    
    /**
    * Feldolgozás indítása.
    * Megkapja a FKI fájlok nevét és FELHKI objektum egyedeket és evvel a tételeket is.
    * 
    * @param felhkiFileok feldolgozott Felhki fájlok
    * @version 1.0
    * @since   2021-05-20
    * @throws IOException
    */
    public void start(Map<String, FELHKI> felhkiFileok) throws IOException
    {
        Iterator it = felhkiFileok.entrySet().iterator();
        XSSFCellStyle borderFormatCell = workbook.createCellStyle();
        borderFormatCell.setBorderBottom(BorderStyle.THIN);
        borderFormatCell.setFillForegroundColor(IndexedColors.BLACK.getIndex());
	borderFormatCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
        XSSFFont font=workbook.createFont();
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        /* Also make the font color to RED */
        font.setColor(XSSFFont.COLOR_RED);
        /* attach the font to the style  created earlier */
        borderFormatCell.setFont(font);
        // Tétel elválasztás
        XSSFCellStyle tetelBorderFormatCell = workbook.createCellStyle();
        tetelBorderFormatCell.setBorderBottom(BorderStyle.THIN);
        // Fájlokból kiszed kiszedjük a FELHKI obejktumot
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry)it.next();
            // System.out.println(pair.getKey() + " = " + pair.getValue());
            felhki = (FELHKI) pair.getValue();
            // Első sor fejléc fájlonként
            createRow();
            int cellid = 0;                    
            Cell cell = row.createCell(cellid++);
            cell.setCellStyle(borderFormatCell);
            cell.setCellValue((String) pair.getKey());
            cell = row.createCell(cellid++);
            cell.setCellStyle(borderFormatCell);
            cell = row.createCell(cellid++);
            cell.setCellStyle(borderFormatCell);
            cell = row.createCell(cellid++);
            cell.setCellStyle(borderFormatCell);
            cell = row.createCell(cellid++);
            cell.setCellValue(felhki.getUzenetSorszamString());
            cell.setCellStyle(borderFormatCell);
            cell = row.createCell(cellid++);
            cell.setCellValue(felhki.getuzenetOsszeallitasDatuma().toString());
            cell.setCellStyle(borderFormatCell);
            cell = row.createCell(cellid++);
            cell.setCellValue(felhki.getUzenetOsszeallitasanakIdeje().toString());
            cell.setCellStyle(borderFormatCell);
            while(cellid<7)
            {
                cell = row.createCell(cellid++);
                cell.setCellStyle(borderFormatCell);
            }
            cell = row.createCell((cellid++));
            cell.setCellValue(felhki.getSzolgaltatoKod());
            cell.setCellStyle(borderFormatCell);

            // Tételek a FELHKI-ből
            ArrayList tetelek = felhki.getFelhkiTetelList();
            Iterator iter = tetelek.iterator();
            while (iter.hasNext()) 
            { 		     
                FELHKI_TETEL tetel = (FELHKI_TETEL) iter.next();
                createRow();
                // Tételek
                cellid = 0;                    
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getTetelSorszam().toString());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFelhatalmazasJellegeString());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFelhatalmazasJellegeMegjegyzessel());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFogyasztoAzonositoja());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getKotelezettBankszamlaSzama());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getKotelezettNeve());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFelhatalmazasErvenyessegenekKezdete());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFelhatalmazasErtekhatara());
                cell = row.createCell(cellid++);
                // Tételek (Új sor)
                cellid = 5;   
                createRow();
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFogyasztoNeve());
                cell = row.createCell(cellid++);
                cell.setCellValue(tetel.getFelhatalmazasErvenyessegenekVege());
                // Tételek (Új sor)
                
                createRow();
                for(cellid = 0;cellid<5;cellid++)
                {
                    cell = row.createCell(cellid);
                    cell.setCellStyle(tetelBorderFormatCell);
                }
                cellid = 5;  
                cell = row.createCell(cellid++);
                cell.setCellStyle(tetelBorderFormatCell);
                cell.setCellValue(tetel.getFogyasztoCim());
                cell = row.createCell(cellid++);
                cell.setCellStyle(tetelBorderFormatCell);
                cell.setCellValue(tetel.getFelhatalmazasKelte());
                cell = row.createCell(cellid++);
                cell.setCellStyle(tetelBorderFormatCell);
            }
        }
            spreadsheet = autoSizeColumns(spreadsheet, 10);
            // Fekvő tájolásra állítás
            spreadsheet.getPrintSetup().setLandscape(true);
            // System.out.println(felhkiHibaList.size());
            /*
            if(felhkiHibaList.size()>0)
            {
                for (String key : felhkiHibaList.keySet())
                {
                    int cellid=0;
                    row = spreadsheet.createRow(rowid++);
                    Cell cell = row.createCell(cellid++);
                    cell.setCellStyle(borderFormatCell);
                    cell.setCellValue(key);
                    cell = row.createCell(cellid++);
                    cell.setCellStyle(borderFormatCell);
                    cell = row.createCell(cellid++);
                    cell.setCellStyle(borderFormatCell);
                    cell = row.createCell(cellid++);
                    cell.setCellStyle(borderFormatCell);
                    cell.setCellValue(felhkiHibaList.get(key));
                    System.out.println("Key: "+key+"value: "+felhkiHibaList.get(key));
                }
            }*/
            
            //Write the workbook in file system
            File outXlsx = new File(outPath+getDate()+".xlsx");
            FileOutputStream out = new FileOutputStream(outXlsx);
            workbook.write(out);
            out.close();
            System.out.println(outPath+getDate()+".xlsx táblázat sikeresen mentve!");
            logger.info(outPath+getDate()+".xlsx táblázat sikeresen mentve!");
    }
    /**
    * Minden oszlopszélesség beállítása.
    * 
    * @param spreadsheet excel munkafüzet
    * @param maxColumn vizsgált oszlopok száma
    * @version 1.0
    * @since   2021-05-20
    */
    public XSSFSheet autoSizeColumns (XSSFSheet spreadsheet, int maxColumn)
    {
        for(int i=0;i<=maxColumn;i++)
        {
            spreadsheet.autoSizeColumn((short) i);
        }
        return spreadsheet;
    }
    
    /**
    * Új sor létrehozása a munkafüzetben.
    * 
    * @version 1.0
    * @since   2021-05-20
    */

    public void createRow()
    {
        row = spreadsheet.createRow(rowid++);       
    }
   
    public String getOutPath() {
        return outPath;
    }

    public void setOutPath(String outPath) {
        this.outPath = outPath;
    }
        
    /**
    * getDate függvény generál nevet a kimeneti fájlhoz.
    * @return String
    */  
    public String getDate()
    {
        Date date = new Date(); // This object contains the current date value
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return formatter.format(date).toString();
    }

    public HashMap<String, String> getFelhkiHibaList() {
        return felhkiHibaList;
    }

    public void setFelhkiHibaList(HashMap<String, String> felhkiHibaList) {
        this.felhkiHibaList = felhkiHibaList;
    }
    
}
      
  