package com.group.felhki_adatok_beolvasas;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/** System.out átirányítása a TextArea-ba
 * 
 * @author peterr
 */
public class JTextAreaOutputStream extends OutputStream
{
    private final JTextArea destination;
     /** System.out átirányítása a TextArea-ba
     * 
     * @param destination TextArea ahova át legyen irányítva
     * @author peterr
     */
    public JTextAreaOutputStream (JTextArea destination)
    {
        if (destination == null)
            throw new IllegalArgumentException ("Destination is null");

        this.destination = destination;
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException
    {
        final String text = new String (buffer, offset, length);
        SwingUtilities.invokeLater(new Runnable ()
            {
                @Override
                public void run() 
                {
                    destination.append (text);
                }
            });
    }

    @Override
    public void write(int b) throws IOException
    {
        write (new byte [] {(byte)b}, 0, 1);
    }
}