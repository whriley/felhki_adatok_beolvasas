﻿$From ='peter.richard@tszol.hu'
$EmailToAddresses = @("peter.richard@tszol.hu","blaskone.zsuzsa@tszol.hu","racz.tibor@tszol.hu")
$SMTP= 'mail.komtavho.hu'

$path = $args[0]
write-host "A kapott fajl: "$path

# Információs pult
Write-host "FELHKI Email PSScript inditasa..."

Function sendEmail([string]$emailFrom, [string]$emailTo, [string]$subject,[string]$body,[string]$smtpServer,[string]$filePath)
{
	#initate message
	$email = New-Object System.Net.Mail.MailMessage 
	$email.From = $emailFrom
	$email.To.Add($emailTo)
	$email.Subject = $subject
	$email.Body = $body
	if($filePath -ne "null")
	{
		# initiate email attachment 
		$emailAttach = New-Object System.Net.Mail.Attachment $filePath
		$email.Attachments.Add($emailAttach) 
		#initiate sending email
	}	
	$smtp = new-object Net.Mail.SmtpClient($smtpServer)
	$smtp.Send($email)
}
foreach ($to in $EmailToAddresses)
{
	Write-host "Levelkuldes inditasa az alabbi cimzettnek: $to"
	#Call Function 
	if($path -eq "null")
	{
		Write-host "Nincs felhki (null) uzenet kuldese!"
		$OFS = "`r`n"
		$msg = "Nem található mai napon feldolgozandó FELHKI fájl! " + $OFS + ""+ $OFS + "Üdvözlettel: Péter Richárd"
		sendEmail -emailFrom $from -emailTo $to -subject "FELHKI lista" -body $msg -smtpServer $SMTP -filePath "null"
	}
	if($path -ne "null")
	{	
		$OFS = "`r`n"
		$msg = "A felhki fájl csatolva!" + $OFS + ""+ $OFS + "Üdvözlettel: Péter Richárd"
		sendEmail -emailFrom $from -emailTo $to -subject "FELHKI lista" -body $msg -smtpServer $SMTP -filePath $path 
	}
}
